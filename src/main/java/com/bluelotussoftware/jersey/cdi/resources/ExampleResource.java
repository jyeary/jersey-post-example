/*
 * Copyright 2013 Blue Lotus Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * $Id$
 */
package com.bluelotussoftware.jersey.cdi.resources;

import com.bluelotussoftware.jersey.cdi.WidgetSingleton;
import com.bluelotussoftware.jersey.misc.Widget;
import java.io.Serializable;
import java.util.List;
import java.util.ListIterator;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import org.apache.commons.codec.binary.Hex;

/**
 * REST Web Service
 *
 * @author John Yeary <jyeary@bluelotussoftware.com>
 */
@Path("example")
@RequestScoped
public class ExampleResource implements Serializable {

    private static final long serialVersionUID = -8883891023988941637L;
    @Inject
    WidgetSingleton ws;
    @Context
    private UriInfo uriInfo;

    public ExampleResource() {
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public GenericEntity<List<Widget>> get() {
        return new GenericEntity<List<Widget>>(ws.getWidgets()) {
        };
    }

    @GET
    @Path("{index}")
    @Produces({MediaType.APPLICATION_JSON})
    public Widget get(@PathParam("index") int index) {
        return ws.getWidgets().get(index);
    }

    /**
     * PUT method for updating or creating an instance of ExampleResource
     *
     * @param widget representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response put(Widget widget) {
        ListIterator<Widget> iterator = ws.getWidgets().listIterator();
        boolean found = false;
        for (Widget w : ws.getWidgets()) {
            if (w.getName() != null && w.getName().equals(widget.getName())) {
                found = true;
                ws.remove(w);
                ws.add(widget);
            }
        }

        // This is not idempotent!
        if (!found) {
            ws.add(widget);
            UriBuilder uriBuilder = UriBuilder.fromUri(uriInfo.getRequestUri());
            uriBuilder.path("{index}");
            String index = String.valueOf(ws.getWidgets().lastIndexOf(widget));
            return Response.created(uriBuilder.build(index)).entity(widget).build();
        }

        return Response.ok(widget).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces({MediaType.APPLICATION_JSON})
    public Response post(@FormParam("name") String name, @FormParam("value") String value) {
        Widget widget = new Widget(name, value);
        ws.add(widget);
        int index = ws.getWidgets().indexOf(widget);
        UriBuilder uriBuilder = UriBuilder.fromUri(uriInfo.getRequestUri());
        uriBuilder.path("{index}");
        EntityTag etag = new EntityTag(Hex.encodeHexString(widget.toString().getBytes()));
        return Response.created(uriBuilder.build(index)).
                tag(etag).
                entity(widget).
                type(MediaType.APPLICATION_JSON).
                build();
    }
}
