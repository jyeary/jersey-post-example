<%-- 
    Document   : index
    Created on : Jan 11, 2012, 8:00:24 AM
    Author     : John Yeary
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Jersey POST Response Example</title>
    </head>
    <body>
        <form id="form1" action="resources/example" method="post" enctype="application/x-www-form-urlencoded">
            <p>
                Name: <input id="name" type="text" name="name"/>
            </p>
            <p>
                Value: <input id="value" type="text" name="value"/>
            </p>
            <input type="submit" value="Submit"/>
        </form>
    </body>
</html>
